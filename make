#!/bin/bash

gcc -c -I /shared/home/aslab/windo/graph/include /shared/home/aslab/windo/graph/src/TG_filedef.c
gcc -c -I /shared/home/aslab/windo/graph/include /shared/home/aslab/windo/graph/src/TG_csr.c
gcc -c -I /shared/home/aslab/windo/graph/include /shared/home/aslab/windo/graph/src/TG_triple.c
gcc -c -I /shared/home/aslab/windo/graph/include /shared/home/aslab/windo/graph/src/agrparse.c
gcc -c -I /shared/home/aslab/windo/graph/include /shared/home/aslab/windo/graph/src/TG_triangle.c
gcc -c -I /shared/home/aslab/windo/graph/include /shared/home/aslab/windo/graph/src/TG_datadef.c
gcc -c -I /shared/home/aslab/windo/graph/include /shared/home/aslab/windo/graph/src/TG_main.c

gcc -o driver  TG_filedef.o TG_datadef.o TG_csr.o TG_triple.o TG_triangle.o agrparse.o TG_main.o

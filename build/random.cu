#include<stdio.h>
#include<stdlib.h>
#include<cuda.h>
#include<curand.h>

int main (int argc, char *argv[] ){
    size_t n = 10;
    size_t i ;
    curandGenerator_t gen;
    float *devData, *hostData;

    // Alokasi n floats host

    hostData = (float *) calloc(n, sizeof(float));


    // alokasi __device__

    cudaMalloc((void **) &devData, n*sizeof(float));

    curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_MTGP32);

    curandSetPseudoRandomGeneratorSeed(gen, 1234ULL);

    curandGenerateUniform(gen, devData, n);

    cudaMemcpy(hostData, devData, n * sizeof(float), cudaMemcpyDeviceToHost);

    printf("Random Unif(0, 1) Draw: \n");

    for (i = 0 ; i < n; i++){
        printf(" %1.4f\n", hostData[i]);
    }
    printf("\n");

    curandDestroyGenerator(gen);
    cudaFree(devData);
    free(hostData);
}

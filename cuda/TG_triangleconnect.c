#include "tgcuda.h"


__global__ void triangleConnection(int * X, int * Y, int * A, int * B, int * fn, int * tn, int v, int e, int nSample, int * tri)
{
      int i = threadIdx.x;
      int j;
      if( i < nSample){ // yang harus dibagi
          if (X[i] == A[i]) {
              for (j = 0 ; j < e; j++){
                  if( (Y[i] == fn[j] ) && (B[i] == tn[j])) {
                      tri[i] = 1;
                  }
              }
          }
      }
}

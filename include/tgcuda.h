#ifndef TGCUDA_H
#define TGCUDA_H

#include "cuda.h"
//#include "cuda_runtime.h"
#include "math.h"
#include "curand.h"
#include "header.h"

// cuda header function

__global__ void getTripleSampling( int nSample, int e, int *fn, int *tn);
__global__ void cuda_triangleConnection(int * X, int * Y, int * A, int * B, int * fn, int * tn, int v, int e, int nSample, int * tri);
__global__ void cuda_tripleCounting(int nTriple, int *de, int v);



#endif

#include "header.h"
#include "tgcuda.h"

int         _edges = 0;
int         _vertex = 0;
float      _sampleRate = 0.0 ;
char  *path = NULL;
char  *_projecName = NULL;


void argumentparsefile(int argc, const char **argv){
  // int force = 0;
  // int test = 0;
  // int num = 0;
  // const char *path = NULL;
  int perms = 0;


  struct argparse_option options[] = {
      OPT_HELP(),
      OPT_GROUP("Basic options"),
      OPT_INTEGER('e', "edges", &_edges, "number of edges"),
      OPT_INTEGER('v', "vertex", &_vertex, "number of vertex"),
      OPT_FLOAT('r', "rate", &_sampleRate, "number of rate sample per 100"),
      OPT_STRING('p', "path", &path, "path to read data"),
      OPT_STRING('n', "name", &_projecName, "selected project name"),
      OPT_GROUP("Bits options"),
      OPT_BIT(0, "read", &perms, "read perm", NULL, PERM_READ, OPT_NONEG),
      OPT_BIT(0, "write", &perms, "write perm", NULL, PERM_WRITE),
      OPT_BIT(0, "exec", &perms, "exec perm", NULL, PERM_EXEC),
      OPT_END(),
  };

  struct argparse argparse;
  argparse_init(&argparse, options, usage, 0);
  argparse_describe(&argparse, "\nA brief description of what the program does and how it works.", "\nAdditional description of the program after the description of the arguments.");
  argc = argparse_parse(&argparse, argc, argv);

  printf("\n=====================================================================\n");
  printf("File            \t: %s \n", _projecName);

  printf("Number of Vertex \t: %d \n", _vertex);

  printf("Number of Edges  \t: %d \n", _edges);

  printf("Sample Rate      \t: %f \n", _sampleRate);
  printf("---------------------------------------------------------------------\n");

  // if (force != 0)
  //     printf("force: %d\n", force);
  // if (test != 0)
  //     printf("test: %d\n", test);
  // if (path != NULL)
  //     printf("path: %s\n", path);
  // if (num != 0)
  //     printf("num: %d\n", num);
  // if (argc != 0) {
  //     printf("argc: %d\n", argc);
  //     int i;
  //     for (i = 0; i < argc; i++) {
  //         printf("argv[%d]: %s\n", i, *(argv + i));
  //     }
  // }
  if (perms) {
        printf("perms: %d\n", perms);
   }

}

int main (int argc, const char **argv){
      argumentparsefile(argc,argv);

      FILE *importFile;

      // int edges = 111;
      // int vertex = 27 ;
      int nTriple;

      int nSample;
      int nde;
      int nSampleNew;
      int tCount ;

      // array allocation for graph coordinate (x,y)
      int* fn = (int*) malloc((_edges)*sizeof(int));
      int* tn = (int*) malloc((_edges)*sizeof(int));

      // array allocation for csr
      int* col_idx =(int*) malloc((_edges)*sizeof(int));
      int* row_ptr =(int*) malloc((_vertex)*sizeof(int));
      int* ncol_idx =(int*) malloc((_vertex)*sizeof(int));
      int* de =(int*) malloc((_vertex)*sizeof(int));

      // start time
      // clock_t start, end;
      // double cpu_time_used;
      // start = clock();


      // load graph coordinate from file
      loadData(&importFile, path);
      grahpExport(importFile, _edges, fn, tn);

      // write again fromNode and toNode graph(x,y) by separate between each with  1 dimension array

      WtoF("fromNode.txt" , _projecName,fn, _edges);
      WtoF("toNode.txt", _projecName,tn, _edges);


      CrE(fn, tn, _vertex, _edges, col_idx, row_ptr, ncol_idx);

      WtoF("col_idx.txt", _projecName, col_idx, _edges);
      WtoF("row_ptr.txt", _projecName, row_ptr, _vertex - 1 );
      WtoF("ncol_idx.txt", _projecName, ncol_idx, _vertex);

      nde = CoD(ncol_idx, row_ptr, _vertex, _edges, de);

      printf("\nNumber of Degree %ld \n", nde);
      printf("---------------------------------------------------------------------\n");
      WtoF("degree.txt", _projecName ,de, _vertex);


      int size_single = sizeof(int);
      int *d_de;
      int *d_vertex, *d_nTriple;


      // allocation space for device copies of de, vertex, nTriple
      cudaMalloc((void**)&d_de, _vertex*sizeof(int));
      cudaMalloc((void**)&d_vertex, size_single);
      cudaMalloc((void**)&d_nTriple, size_single);

      cudaMemcpy(d_de, &de, _vertex*sizeof(int), cudaMemcpyHostToDevice);
      cudaMemcpy(d_vertex, &_vertex, sizeof(int), cudaMemcpyHostToDevice);

      cuda_tripleCounting <<< 1, 1 >>> (nTriple, d_de, _vertex);

      cudaMemcpy(&nTriple, d_nTriple, size_single, cudaMemcpyDeviceToHost);

      //cudaDeviceSynchronize();
      /// ----------------------------------------------------

      printf("Number of triple %d \n", nTriple);


}

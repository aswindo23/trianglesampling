int         _edges = 0;
int         _vertex = 0;
float      _sampleRate = 0.0 ;
const char  *_path = NULL;
const char  *_projecName = NULL;
void argumentparsefile(int argc, const char **argv){
  // int force = 0;
  // int test = 0;
  // int num = 0;
  // const char *path = NULL;
  int perms = 0;


  struct argparse_option options[] = {
      OPT_HELP(),
      OPT_GROUP("Basic options"),
      OPT_INTEGER('e', "edges", &_edges, "number of edges"),
      OPT_INTEGER('v', "vertex", &_vertex, "number of vertex"),
      OPT_FLOAT('r', "rate", &_sampleRate, "number of rate sample per 100"),
      OPT_STRING('p', "path", &_path, "path to read data"),
      OPT_STRING('n', "name", &_projecName, "selected project name"),
      OPT_GROUP("Bits options"),
      OPT_BIT(0, "read", &perms, "read perm", NULL, PERM_READ, OPT_NONEG),
      OPT_BIT(0, "write", &perms, "write perm", NULL, PERM_WRITE),
      OPT_BIT(0, "exec", &perms, "exec perm", NULL, PERM_EXEC),
      OPT_END(),
  };

  struct argparse argparse;
  argparse_init(&argparse, options, usage, 0);
  argparse_describe(&argparse, "\nA brief description of what the program does and how it works.", "\nAdditional description of the program after the description of the arguments.");
  argc = argparse_parse(&argparse, argc, argv);
  // if (force != 0)
  //     printf("force: %d\n", force);
  // if (test != 0)
  //     printf("test: %d\n", test);
  // if (path != NULL)
  //     printf("path: %s\n", path);
  // if (num != 0)
  //     printf("num: %d\n", num);
  // if (argc != 0) {
  //     printf("argc: %d\n", argc);
  //     int i;
  //     for (i = 0; i < argc; i++) {
  //         printf("argv[%d]: %s\n", i, *(argv + i));
  //     }
  // }
  if (perms) {
        printf("perms: %d\n", perms);
   }

}

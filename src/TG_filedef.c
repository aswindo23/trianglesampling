#include "header.h"


void loadData(FILE** importFile ,char fileName[]){
      *importFile = fopen(fileName, "r");
      if (*importFile != NULL){
        printf("File Successfully opened a. \n");
      }
}

void WtoF(char fileName[], char projectname[]  , int * mat1d, int n){
      char Path[256];
      int i;
      strcpy(Path, "output/");
      strcat(Path, projectname);
      strcat(Path, "_");
      strcat(Path, fileName);


      FILE *exportFile= fopen(Path, "w");
      for(i = 0 ; i < n ; i++){
          fprintf(exportFile, "%ld\n", mat1d[i]); // flicker di ld in dulu
      }
      fclose(exportFile);

}

void V2WtoF(char fileName[], char projectname[]  , int *fn , int *tn, int n){
      char Path[256];
      int i;
      strcpy(Path, "output/");
      strcat(Path, projectname);
      strcat(Path, "_");
      strcat(Path, fileName);


      FILE *exportFile= fopen(Path, "w");
      for(i = 0 ; i < n ; i++){
          fprintf(exportFile, "%ld %ld\n", fn[i] , tn[i]); // flicker di ld in dulu
      }
      fclose(exportFile);

}

void grahpExport(FILE *importFile, int edges, int *fn, int *tn){
      int number, i;
      if (importFile == NULL){
          printf("File not exits. \n");
      }else{
       for(i = 0 ; i < edges ; i++ ){
         if(!fscanf(importFile, "%ld %ld", &fn[i], &tn[i] )){
           break;
         }
       }
      }
      fclose(importFile);
 }

 void deR(FILE *importFile, int *de, int v){
    int i;
    if(importFile == NULL){
      printf(" File not Exist");
    }else{
        for(i = 0 ; i < v  ; i++){
            if(!fscanf(importFile, "%d", &de[i])){

                break;
            }
        }
    }
    fclose(importFile);
 }


 void getTripleConnection(char fileName[], char projectname[], int nSample, int *X, int *Y, int *A, int *B){

   // path
   FILE *importFile;
   char Path[256];
   int i;
   strcpy(Path, "output/");
   strcat(Path, projectname);
   strcat(Path, "_");
   strcat(Path, fileName);

   loadData(&importFile, Path);

   if (importFile == NULL){
     printf("File not exits. \n");
   }else{
     for(i = 0 ; i < nSample ; i++ ){
       if(!fscanf(importFile, "%ld\t%ld\t%ld\t%ld", &X[i], &Y[i], &A[i], &B[i]  )){
         break;
       }
     }
   }
   fclose(importFile);
 }


 void WtriangleToFile(int * X, int * Y, int * A, int * B, int * tri, int nSample, char fileName[], char projectname[]){

    // Path
    char Path[256];
    int i;
    strcpy(Path, "output/");
    strcat(Path, projectname);
    strcat(Path, "_");
    strcat(Path, fileName);


     FILE *exportFile = fopen(Path, "w");
     for(i = 0 ; i < nSample; i++){
         fprintf(exportFile, "%ld\t%ld\t%ld\t%ld\t%ld\n",  X[i], Y[i], A[i], B[i], tri[i]);
     }
     fclose(exportFile);
 }


 void Zeros1DM(int * arr, int number){
      int i;
     for(i = 0 ; i < number; i ++ ){
         arr[i] = 0;
     }
 }

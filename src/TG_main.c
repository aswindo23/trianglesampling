#include "header.h"

// Global Variable for TG_main.c file


int         edges = 0;
int         vertex = 0;
float      sampleRate = 0.0 ;
char  *path = NULL;
char  *exec = NULL;
char  *projecName = NULL;
void argumentparsefile(int argc, const char **argv){

  int perms = 0;

  // Create execute as
  // 1. csr { get ncol, row_ptr, col_idx, degree, triple number }
  // 2. trp { triple sampling }
  // 3. tri { triangle sampling }

  struct argparse_option options[] = {
      OPT_HELP(),
      OPT_GROUP("Basic options"),
      OPT_INTEGER('e', "edges", &edges, "number of edges"),
      OPT_INTEGER('v', "vertex", &vertex, "number of vertex"),
      OPT_FLOAT('r', "rate", &sampleRate, "number of rate sample per 100"),
      OPT_STRING('p', "path", &path, "path to read data"),
      OPT_STRING('p', "exec", &exec, "execute as { 'csr','trp','trg' }"),
      OPT_STRING('n', "name", &projecName, "selected project name"),
      OPT_GROUP("Bits options"),
      OPT_BIT(0, "read", &perms, "read perm", NULL, PERM_READ, OPT_NONEG),
      OPT_BIT(0, "write", &perms, "write perm", NULL, PERM_WRITE),
      OPT_BIT(0, "exec", &perms, "exec perm", NULL, PERM_EXEC),
      OPT_END(),
  };

  struct argparse argparse;
  argparse_init(&argparse, options, usage, 0);
  argparse_describe(&argparse, "\nA brief description of what the program does and how it works.", "\nAdditional description of the program after the description of the arguments.");
  argc = argparse_parse(&argparse, argc, argv);

  printf("\n=====================================================================\n");
  printf("File            \t: %s \n", projecName);

  printf("Number of Vertex \t: %d \n", vertex);

  printf("Number of Edges  \t: %d \n", edges);

  printf("Sample Rate +     \t: %f \n", sampleRate);
  printf("---------------------------------------------------------------------\n");

  // if (force != 0)
  //     printf("force: %d\n", force);
  // if (test != 0)
  //     printf("test: %d\n", test);
  // if (path != NULL)
  //     printf("path: %s\n", path);
  // if (num != 0)
  //     printf("num: %d\n", num);
  // if (argc != 0) {
  //     printf("argc: %d\n", argc);
  //     int i;
  //     for (i = 0; i < argc; i++) {
  //         printf("argv[%d]: %s\n", i, *(argv + i));
  //     }
  // }
  if (perms) {
        printf("perms: %d\n", perms);
   }

}



int main (int argc, const char **argv){

    // argument call
    argumentparsefile(argc,argv);


  FILE *importFile;

  // int edges = 111;
  // int vertex = 27 ;
  long int nTriple = 0;
  //long int nTriple = 0;
  long int ntall;
  int nSample;
  int nde;
  int nSampleNew;
  int tCount;
  long int ntsam;
  int updateEdges;
  // array allocation for graph coordinate (x,y)
  int* fn = malloc((edges)*sizeof(int*));
  int* tn = malloc((edges)*sizeof(int*));

  // array allocation for csr
   int* col_idx = malloc((edges)*sizeof(int*));
   int* row_ptr = malloc((vertex)*sizeof(int*));
   int* ncol_idx = malloc((vertex)*sizeof(int*));
   int* de = malloc((vertex)*sizeof(int*));

  // start time
  clock_t start, end;
  double cpu_time_used;
  start = clock();


  // load graph coordinate from file
  loadData(&importFile, path); // __f
  grahpExport(importFile, edges, fn, tn);  // __f

  // int max_fn;
  // int max_tn;
  //
  // max_fn = max(fn,  edges);
  // //max_tn = max(tn,  edges);
  // //max_tn = max(tn,  vertex);
  // // write again fromNode and toNode graph(x,y) by separate between each with  1 dimension array
  // printf("Nilai Max fn %d \n", max_fn);
  // printf("Nilai Max tn %d \n", max_tn);
  //  updateEdges = getUpperTrangleMat(importFile,projecName,"data.txt",fn,tn,edges);
  //  printf("updated edges %d ", updateEdges);
  //  V2WtoF("__data.txt", projecName , fn , tn, updateEdges);
  // // V2WtoF("__data.txt", projecName  , fn, tn,  updateEdges);
  //
  WtoF("fromNode.txt" , projecName,fn, edges);
  WtoF("toNode.txt", projecName,tn, edges);
  //
  //
  CrE(fn, tn, vertex, edges, col_idx, row_ptr, ncol_idx); // __f
  // //
  WtoF("col_idx.txt", projecName, col_idx, edges);
  WtoF("row_ptr.txt", projecName, row_ptr, vertex - 1 );
  WtoF("ncol_idx.txt", projecName, ncol_idx, vertex);
  // //
  printf("check row pt %d",row_ptr[8]);
  nde = CoD(ncol_idx, row_ptr, vertex, edges, de);
  // //
  printf("\nNumber of Degree %ld \n", nde);
  // // printf("---------------------------------------------------------------------\n");
  WtoF("degree.txt", projecName ,de, vertex);
  // //
  nTriple =  triple_counting(de, vertex, edges);
  // // printf("---------------------------------------------------------------------\n");
  printf("Number of triple %ld \n", nTriple);
  // //
  //  //get number sample of triple population at graph
  nSample = numberSampling(nTriple, sampleRate);
  //  printf("---------------------------------------------------------------------\n");
  printf("Number of Sample %ld \n", nSample);
  //
  // // get triple sample
   nSampleNew = getTripleSampling( projecName ,nSample, vertex, edges,fn,tn );
  //
  // // update number of sampling
   nSample = nSampleNew;
  // printf("---------------------------------------------------------------------\n");
   printf("Update Number of Sample %d \n", nSample);
  //
  //
  //
  // // Check Triangle
  FILE *importTripleSample;
  //
   int* X = malloc((nSample)*sizeof(int*));
   int* Y = malloc((nSample)*sizeof(int*));
  int* A = malloc((nSample)*sizeof(int*));
   int* B = malloc((nSample)*sizeof(int*));
  int* tri = malloc((nSample)*sizeof(int*));
   float transEst = 0;
  int triEst = 0;
  //
  // // ----- path for triple sample
  //
  // //loadData(&importTripleSample, "output/zebra_");
   getTripleConnection("tripleSample.txt", projecName, nSample, X, Y, A, B);
   Zeros1DM(tri, nSample);
   //tCount = triangle_connection(X, Y,  A,  B,  fn, tn, vertex, edges,  nSample, tri);
   traingleConnection(X, Y,  A,  B,  fn, tn, vertex, edges,  nSample, tri, &tCount, de, &ntsam, &ntall);

   printf("---------------------------------------------------------------------\n");
   printf("Number of Triangle %d \n", tCount);
   printf("Number of Triangle %ld \n", ntsam);
  //
  // // write triangle to file
  // tCount = number of triangle counting 
  // ntsam  = number of triple sample from triangle 
  // nSample = number of triple sample
  // nTriple = number of triple at graph
  //
  WtriangleToFile(X, Y, A, B, tri, nSample,"Triangle_Connection.txt", projecName);
  //
   transEst = transitivityEstimation(ntsam, ntall);
   triEst = triangleEstimation(transEst, nTriple);
  // printf("---------------------------------------------------------------------\n");
   printf("Transitivity Estimation : %f \n ",transEst);
  // printf("---------------------------------------------------------------------\n");
   printf("Triangle Estimation : %ld \n",triEst);
  // printf("---------------------------------------------------------------------\n");
  // // end time
  //
  end = clock();
  cpu_time_used = ((double) (end - start)) / (double) CLOCKS_PER_SEC;
   printf("---------------------------------------------------------------------\n");
   printf("Time Execution: %f \n",cpu_time_used);
   printf("=====================================================================\n");

  // free(X);
  // free(Y);
  // free(A);
  //  free(B);
   free(fn);
   free(tn);
   //free(col_idx);
   //free(row_ptr);
   //free(ncol_idx);
  // free(de);
    // free(tri);

  return 0;

}

# TriangleSampling

TriangleSampling Project

### How to Install Apps

check before compile this application

```sh
$ git clone https://gitlab.com/aswindo23/trianglesampling.git 
$ cd trianglesampling 
$ mkdir data 
$ cd data && wget data link && cd ..
$ mkdir output
```

compile serial gcc  

```sh 
$ CC=gcc sh make
```

compile cuda 

```sh
$ NCC=nvcc CC=gcc sh make_cuda
```